package com.example.maiq.myapplication;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.Image;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

import static com.example.maiq.myapplication.Modifier.EMPTY;
import static com.example.maiq.myapplication.Modifier.TOWER;
import static com.example.maiq.myapplication.Modifier.SKULL;
import static com.example.maiq.myapplication.Modifier.SWORD;

public class MainActivity extends AppCompatActivity {

    private ViewGroup viewGroup;
    private TextView textView;
    private ImageView defenderView,attackerView,cloudButton;
    TransitionDrawable td;
    TransitionDrawable td2;
    drawResult wynikBogow;
    chaosGenerator bogowie;
    boolean thirdTaken = true;
    boolean attackerTaken = true;
    boolean defenderTaken = true;

    HashMap<Card,Drawable> map = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        defenderView = (ImageView) findViewById(R.id.defenderView);
        defenderView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!defenderTaken) {
                    revealCard(defenderView, wynikBogow.getFirstDraw());
                    defenderTaken = true;
                }
                else if (!thirdTaken) {
                    revealCard(defenderView, wynikBogow.getThirdDraw());
                    thirdTaken = true;
                }
                else
                {
                    /* do nothing */
                }

            }
        });


        attackerView = (ImageView) findViewById(R.id.attackerView);
        attackerView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!attackerTaken){
                    revealCard(attackerView,wynikBogow.getSecondDraw());
                    attackerTaken = true;
                }
                else if(!thirdTaken){
                    revealCard(attackerView,wynikBogow.getThirdDraw());
                    thirdTaken = true;
                }
                else{
                    /* do nothing */
                }

            }
        });



        populateMap();


        cloudButton = (ImageView) findViewById(R.id.cloudButton);
        cloudButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                animateCloud();
                startDrawingCards();

            }
        });
    }


    private void populateMap()
    {
        map.put(new Card(0,EMPTY),getResources().getDrawable(R.drawable.empty0));
        map.put(new Card(0,SKULL),getResources().getDrawable(R.drawable.skull0));
        map.put(new Card(1,SWORD),getResources().getDrawable(R.drawable.sword1));
        map.put(new Card(1,TOWER),getResources().getDrawable(R.drawable.tower1));
        map.put(new Card(2,EMPTY),getResources().getDrawable(R.drawable.empty2));
        map.put(new Card(3,EMPTY),getResources().getDrawable(R.drawable.empty3));
    }


    private void startDrawingCards()
    {

        defenderTaken = false;
        attackerTaken = false;
        thirdTaken = false;

        wynikBogow = bogowie.start();


        TransitionDrawable td = new TransitionDrawable( new Drawable[] {defenderView.getDrawable(),
                getResources().getDrawable(R.drawable.reverse)
        });

        TransitionDrawable td2 = new TransitionDrawable( new Drawable[] {
                attackerView.getDrawable(),
                getResources().getDrawable(R.drawable.reverse)
        });

            defenderView.setImageDrawable(td);
            attackerView.setImageDrawable(td2);

            td.startTransition(1000);
            td2.startTransition(1000);



    }

    private void revealCard(ImageView v, Card c)
    {
        TransitionDrawable td = new TransitionDrawable( new Drawable[] {v.getDrawable(),
                map.get(c)
        });
        v.setImageDrawable(td);
        td.startTransition(1000);
    }

    private void animateCloud(){

        Drawable[] cloudArray = {getResources().getDrawable(R.drawable.cloud0),
                getResources().getDrawable(R.drawable.cloud1),
                getResources().getDrawable(R.drawable.cloud2),
                getResources().getDrawable(R.drawable.cloud3),
                getResources().getDrawable(R.drawable.cloud4),
                getResources().getDrawable(R.drawable.cloud5),
                getResources().getDrawable(R.drawable.cloud0)
        };

        AnimationDrawable ad = new AnimationDrawable();


        for(int i = 0; i < cloudArray.length ; i++)
        {
            ad.addFrame(cloudArray[i],250);
        }

            cloudButton.setImageDrawable(ad);
            ad.setOneShot(true);
            ad.start();

    }


}

