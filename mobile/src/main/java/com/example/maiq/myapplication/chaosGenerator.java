package com.example.maiq.myapplication;

enum Modifier{
    EMPTY,TOWER,SWORD,SKULL;
};

class Card{
    int value;
    Modifier modifier;

    public Card(int v, Modifier m)
    {
        value = v;
        modifier = m;
    };

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Card)) {
            return false;
        }
        Card cc = (Card) o;
        return ((cc.value == value) && (cc.modifier == modifier));
    }

    public int hashCode() {
        int result = 17;
        result = 31 * result + value + (3* ( (Integer)modifier.ordinal() +5) );
        return result;
    }



}


class Deck{

    Card[] cardDeck = new Card[24];
    public Deck()
    {
        Modifier tmpMod;
        int tmpVal = 0;
        for(int i = 0 ; i < 10 ; i++ ) // generator zer;
        {
            if(i<2)
            {
                tmpMod = Modifier.SKULL;
            }
            else
            {
                tmpMod = Modifier.EMPTY;
            }
            cardDeck[i] =  new Card(tmpVal, tmpMod);

        }
        tmpVal = 1;
        for(int i = 10; i < 18; i++) //generator jedynek;
        {

            if(i<14)
            {
                tmpMod = Modifier.TOWER;
            }
            else
            {
                tmpMod = Modifier.SWORD;
            }
            cardDeck[i] = new Card(tmpVal,tmpMod);
        }

        tmpMod = Modifier.EMPTY;
        tmpVal = 2;
        for(int i = 18; i < 22; i++) //generator dwojek;
        {
            cardDeck[i] = new Card(tmpVal,tmpMod);
        }

        tmpVal = 3;
        for(int i = 22 ; i < 24; i++) //generator trojek;
        {
            cardDeck[i] = new Card(tmpVal,tmpMod);
        }


    }

};

class drawResult{

    Card firstDraw;
    Card secondDraw;
    Card thirdDraw;

    drawResult(Card a, Card b, Card c)
    {
        firstDraw = a;
        secondDraw = b;
        thirdDraw = c;
    }

    public Card getFirstDraw(){
        return firstDraw;
    }

    public Card getSecondDraw(){
        return secondDraw;
    }
    public Card getThirdDraw(){
        return thirdDraw;
    }
}

public class chaosGenerator{
    public static drawResult start()
    {
        Deck d = new Deck();
        double x = 24*Math.random();
        int firstDraw = (int) Math.floor(x);
        int secondDraw;
        int thirdDraw;

        do
        {
            x = 24*Math.random();
            secondDraw = (int) Math.floor(x);
        } while(firstDraw == secondDraw);

        do
        {
            x = 24*Math.random();
            thirdDraw = (int) Math.floor(x);
        } while(thirdDraw == secondDraw || thirdDraw ==firstDraw);

        drawResult result = new drawResult(d.cardDeck[firstDraw],d.cardDeck[secondDraw],d.cardDeck[thirdDraw]);

        return result;
    }
}